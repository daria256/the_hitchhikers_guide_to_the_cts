
### Goals
- The trainee will understand the concept of Bonds and Teams in Linux

### Tasks
- Read about Bonds and Teams in Linux, what are their purposes?
- What are the differences between a Bond and a Team in Linux?
