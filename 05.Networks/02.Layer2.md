
### Goals
- The trainee will get familiar with layer 2 of the tcp/ip model
- The trainee will also get to know few linux commands regarding layer 2

### Tasks
- Explain what are MAC addresses, the structure of MAC addresses and why are they needed
- Explain in your own words what are broadcast domains, why would we like to control their sizes and a method to limit the size of a broadcast domain
- Explain in your own words what are collision domains, explain how can we "break" a collision domain into smaller collision domains
- Switches
  - Why do we need a central network device to connect to?
- Give an example for proper use of VLANs in a LAN environment
- Explain what is MTU, and how does it effect the network traffic
- In Linux, how can you check the MAC address and MTU that is configured on each of the network interfaces?
