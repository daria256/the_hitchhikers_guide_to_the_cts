### Goals
- The trainee will understand about Block Storage & Object Storage

### Tasks
- Read about:
  - Block Storage
  - Object Storage
- Answer:
  - What are the differences between Block Storage and Object Storage?
